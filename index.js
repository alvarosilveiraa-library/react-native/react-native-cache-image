import React from 'react';
import PropTypes from 'prop-types';
import {
  Platform,
  Image,
  Animated,
  ImageBackground
} from 'react-native';
import RNFS from 'react-native-fs';
import md5 from 'md5';
import mime from 'mime';
import axios from 'axios';

const PREFIX = (Platform.OS === 'android')? 'file://': '';

const getPath = (uri, extension) => {
  return `${PREFIX}${RNFS.CachesDirectoryPath}/${md5(uri)}.${extension}`;
}

const getExtensionAsync = async (uri) => {
  const response = await axios(uri);

  const type = response.headers['content-type']
    || response.headers['Content-Type']
    || response.headers['CONTENT-TYPE']
    || 'image/jpeg';

  return mime.getExtension(type);
}

export const downloadImageAsync = async (uri, extension) => {
  const path = getPath(uri, extension);

  const exists = await RNFS.exists(path);

  if(exists) return;

  return RNFS.downloadFile({
    fromUrl: uri,
    toFile: path,
    connectionTimeout: 1000 * 10
  }).promise;
}

export default class CacheImage extends React.Component {
  static defaultProps = {
    animated: false
  };

  static propTypes = {
    style: PropTypes.any,
    source: PropTypes.any.isRequired,
    resizeMode: PropTypes.string,
    animated: PropTypes.bool,
    children: PropTypes.any
  };

  state = {
    source: null
  };

  componentDidMount(){
    this.initialize();
  }

  componentDidUpdate(prevProps) {
    if(prevProps.source !== this.props.source)
      this.initialize();
  }

  async initialize() {
    const source = this.props.source;

    this.setState({ source });

    if(typeof source !== 'object' || !source.uri) return;

    const extension = await getExtensionAsync(source.uri);

    const path = getPath(source.uri, extension);

    const exists = await RNFS.exists(path);

    if(exists) return this.setState({ source: { uri: path } });

    downloadImageAsync(source.uri, extension);
  }

  render() {
    const {
      animated,
      children
    } = this.props;

    const source = this.state.source;

    if(animated) {
      if(children) {
        return (
          <Animated.ImageBackground
            {...this.props}
            source={source}
          >
            {children}
          </Animated.ImageBackground>
        );
      }

      return (
        <Animated.Image
          {...this.props}
          source={source}
        />
      );
    }

    if(children) {
      return (
        <ImageBackground
          {...this.props}
          source={source}
        >
          {children}
        </ImageBackground>
      );
    }

    return  (
      <Image
        {...this.props}
        source={source}
      />
    );
  }
}
