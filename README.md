# react-native-cache-image
> React Native Cache Image for Android and iOS

## Getting started
`$ npm i react-native-fs --save`

`$ react-native link react-native-fs`

`$ npm i react-native-cache-image --save`

## Usage
```javascript
import CacheImage, {
  downloadImageAsync
} from 'react-native-cache-image';

const URI = 'https://domain/image';

// FUNCTION TO CACHE (NOT REQUIRED)
downloadImageAsync(URI)
  .then(res => console.log(res))
  .catch(err => console.log(err));

// COMPONENT
<CacheImage
  style={{ width: 100, height: 100 }}
  source={{ uri: URI }}
  resizeMode="cover"
/>
```
